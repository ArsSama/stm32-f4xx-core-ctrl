<!--
 * @Author: ArsSama 1424838093@qq.com
 * @Date: 2023-06-21 23:55:17
 * @LastEditors: ArsSama 1424838093@qq.com
 * @LastEditTime: 2023-07-01 19:38:33
 * @FilePath: \stm32-f4xx-core-ctrl-master\README.md
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
-->
# STM32-F4xx-CORE-CTRL

#### 介绍
F411CEU6核心控制板

实物图  
![Picture](hardware/image/Picture.jpg)  
作为U盘  
![Flash-U盘](hardware/image/Flash-U盘.jpg)  
3D图（正面）  
![ModleF](hardware/image/Modle-F.jpg)  
3D图（背面）  
![ModleF](hardware/image/Modle-B.jpg)  
Layout  
![Layout](hardware/image/Layout.jpg)

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
