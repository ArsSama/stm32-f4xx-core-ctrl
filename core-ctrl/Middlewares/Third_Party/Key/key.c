/*
 * @Author: ArsSama 1424838093@qq.com
 * @Date: 2023-06-22 22:56:01
 * @LastEditors: ArsSama 1424838093@qq.com
 * @LastEditTime: 2023-06-24 13:54:38
 * @FilePath: \stm32-f4xx-core-ctrl-master\core-ctrl\Middlewares\Third_Party\Key\key.c
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
#include "key.h"
#include "log.h"

const static char *TAG = "key.c";

/**
 * @brief Get the key val object
 *
 * @param ch
 * @return true
 * @return false
 */
static bool get_key_val(KEY_GPIO *key_gpio, uint8_t ch)
{
    switch (ch)
    {
    case 0:
        return HAL_GPIO_ReadPin(key_gpio->GPIO_Up_Port, key_gpio->GPIO_Up_Pin);
    case 1:
        return HAL_GPIO_ReadPin(key_gpio->GPIO_Enter_Port, key_gpio->GPIO_Enter_Pin);
    case 2:
        return HAL_GPIO_ReadPin(key_gpio->GPIO_Down_Port, key_gpio->GPIO_Down_Pin);
    default:
        return false;
    }
}

/**
 * @brief key_scan
 *
 * @param key_msg -- key's msg
 * @param key -- key total num
 */
void key_scan(KEY_GPIO *key_gpio, KEY_MSG *key_msg, KEY *key)
{
    for (uint8_t i = 0; i < sizeof(key); ++i)
    {
        key[i].val = get_key_val(key_gpio, i); /* 获取键值 */
        if (key[i].last_val != key[i].val)     /* 发生改变 */
        {
            key[i].last_val = key[i].val; /* 更新状态 */
            if (key[i].val == 0)
            {
                key_msg->id = i;
                key_msg->pressed = true;
            }
        }
    }
}

/**
 * @brief key_test -- 按键测试
 *
 * @param key_msg -- key's msg
 */
void key_test(KEY_MSG *key_msg)
{
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_SET);
    if (key_msg->pressed)
    {
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_RESET);
        key_msg->pressed = false;
        switch (key_msg->id)
        {
        case 0:
            MY_LOGI(TAG, "KEY Valu-> %d\r\n", key_msg->id);
            break;
        case 1:
            MY_LOGI(TAG, "KEY Valu-> %d\r\n", key_msg->id);
            break;
        case 2:
            MY_LOGI(TAG, "KEY Valu-> %d\r\n", key_msg->id);
            break;
        default:
            break;
        }
    }
}
