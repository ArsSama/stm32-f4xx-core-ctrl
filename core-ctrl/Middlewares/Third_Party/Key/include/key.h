/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : key.h
 * @brief          : Header for main.c file.
 *                   This file contains the common defines of the application.
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2023 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __KEY_H
#define __KEY_H

#ifdef __cplusplus
extern "C"
{
#endif

    /* Includes ------------------------------------------------------------------*/
#include "stdbool.h"
#include "stdint.h"
#include "stm32f4xx_hal.h"
    /* Private includes ----------------------------------------------------------*/
    /* USER CODE BEGIN Includes */

    /* USER CODE END Includes */

    /* Exported types ------------------------------------------------------------*/
    /* USER CODE BEGIN ET */

/*
@param   按键数量
*/
#define KEY_NUM 3

    /**
     * @brief  KEY_STRUCT
     * @param  GPIO_Up_Port
     * @param  GPIO_Up_Pin
     * @param  GPIO_Enter_Port
     * @param  GPIO_Enter_Pin
     * @param  GPIO_Down_Port
     * @param  GPIO_Down_Pin
     */
    typedef struct
    {
        GPIO_TypeDef *GPIO_Up_Port;
        uint16_t GPIO_Up_Pin;
        GPIO_TypeDef *GPIO_Enter_Port;
        uint16_t GPIO_Enter_Pin;
        GPIO_TypeDef *GPIO_Down_Port;
        uint16_t GPIO_Down_Pin;
    } KEY_GPIO;

    /**
     * @brief KEY
     * @param val
     * @param last_val
     */
    typedef struct
    {
        bool val;
        bool last_val;
    } KEY;

    /**
     * @brief KEY_MSG
     * @param id
     * @param pressed
     */
    typedef struct
    {
        uint8_t id;
        bool pressed;
    } KEY_MSG;

    /* USER CODE END ET */

    /* Exported constants --------------------------------------------------------*/
    /* USER CODE BEGIN EC */

    void key_scan(KEY_GPIO *key_gpio, KEY_MSG *key_msg, KEY *key);
    void key_test(KEY_MSG *key_msg);

    /* USER CODE END EC */

    /* Exported macro ------------------------------------------------------------*/
    /* USER CODE BEGIN EM */

    /* USER CODE END EM */

    /* Exported functions prototypes ---------------------------------------------*/

    /* USER CODE BEGIN EFP */

    /* USER CODE END EFP */

    /* Private defines -----------------------------------------------------------*/

    /* USER CODE BEGIN Private defines */

    /* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __KEY_H */
