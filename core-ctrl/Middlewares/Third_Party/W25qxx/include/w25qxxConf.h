/*
 * @Author: ArsSama 1424838093@qq.com
 * @Date: 2023-06-24 14:26:54
 * @LastEditors: ArsSama 1424838093@qq.com
 * @LastEditTime: 2023-06-24 14:41:18
 * @FilePath: \stm32-f4xx-core-ctrl-master\core-ctrl\Middlewares\Third_Party\W25qxx\include\w25qxxConf.h
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
#ifndef _W25QXXCONFIG_H
#define _W25QXXCONFIG_H

#define _W25QXX_SPI                   hspi1
#define _W25QXX_CS_GPIO               GPIOC
#define _W25QXX_CS_PIN                GPIO_PIN_13
#define _W25QXX_USE_FREERTOS          1
#define _W25QXX_DEBUG                 1

#endif
