#ifndef __MUTEX_H
#define __MUTEX_H

#include <stdio.h>
#include "FreeRTOS.h"
#include "semphr.h"

extern SemaphoreHandle_t logMutex;  /*log资源互斥锁*/

void create_mutex(void);

#endif
