/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MENU_H
#define __MENU_H

#include "key.h"

/* 注意对齐 */
#pragma pack(1)
typedef struct
{
    /* 菜单待显示数据存储 */
    struct MENU_A
    {
        uint16_t a_num;
        float adc_channel1;
        float adc_channel2;
    } menu_a;
    struct MENU_B
    {
        uint8_t b_num;
    } menu_b;
    struct MENU_C
    {
        uint8_t c_num;
    } menu_c;
} menu_show_data_t;
#pragma pack()

extern menu_show_data_t menu_show_date;

void main_menu(KEY_MSG *key_msg, menu_show_data_t *menu_data);

#endif /* __MENU_H */
