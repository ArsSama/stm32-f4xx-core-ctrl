/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : log.h
 * @brief          : Header for log.c file.
 *                   This file contains the common defines of the application.
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2023 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LOG_H
#define __LOG_H

#ifdef __cplusplus
extern "C"
{
#endif

    /* Includes ------------------------------------------------------------------*/

    /* Private includes ----------------------------------------------------------*/
    /* USER CODE BEGIN Includes */

    /* USER CODE END Includes */

    /* Exported types ------------------------------------------------------------*/
    /* USER CODE BEGIN ET */

    // 定义互斥锁

    // 定义日志级别枚举
    typedef enum
    {
        MY_LOG_NONE = 0,
        MY_LOG_ERROR,
        MY_LOG_WARN,
        MY_LOG_INFO,
        MY_LOG_DEBUG,
        MY_LOG_VERBOSE
    } my_log_level_t;

    /* USER CODE END ET */

    /* Exported constants --------------------------------------------------------*/
    /* USER CODE BEGIN EC */

    /* USER CODE END EC */

    /* Exported macro ------------------------------------------------------------*/
    /* USER CODE BEGIN EM */

    /* USER CODE END EM */

    /* Exported functions prototypes ---------------------------------------------*/
    void my_log_printf(const char *tag, my_log_level_t level, const char *format, ...);
    void error_handler_msg_log(char *file_msg, const char *func_msg);
    void error_handler(void);
    /* USER CODE BEGIN EFP */

    /* USER CODE END EFP */

    /* Private defines -----------------------------------------------------------*/
    /* USER CODE BEGIN Private defines */

    /* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#define MY_LOGI(tag, format, ...) my_log_printf(tag, MY_LOG_INFO, format, ##__VA_ARGS__)

#endif /* __LOG_H */
