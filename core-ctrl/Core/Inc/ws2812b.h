/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : ws2812.h
 * @brief          : Header for ws2812.c file.
 *                   This file contains the common defines of the application.
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2023 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __WS2812B_H
#define __WS2812B_H

#ifdef __cplusplus
extern "C"
{
#endif

    /* Includes ------------------------------------------------------------------*/

    /* Private includes ----------------------------------------------------------*/
    /* USER CODE BEGIN Includes */
#include "stm32f4xx_hal.h"
    /* USER CODE END Includes */

    /* Exported types ------------------------------------------------------------*/
    /* USER CODE BEGIN ET */

#define PI 3.14159265
#define MAX_LED_NUM 8    /* 最大灯珠数量 */
#define USE_BRIGHTNESS 1 /* 是否开启调节亮度 */

    /* USER CODE END ET */

    /* Exported constants --------------------------------------------------------*/
    /* USER CODE BEGIN EC */

    /**
     * @brief WS2812B
     * @param LED_Data[MAX_LED_NUM][24] 存储灯珠颜色的数据
     * @param LED_Mod[MAX_LED_NUM][24] 存储灯珠亮度的数据
     */
    typedef struct
    {
        uint8_t LED_Data[MAX_LED_NUM][4];
        uint8_t LED_Mod[MAX_LED_NUM][4]; // for brightness
    } WS2812B;

    void set_brightness(WS2812B *ws2812, int brightness);
    void set_led(WS2812B *ws2812, uint8_t led_num, uint8_t Red, uint8_t Green, uint8_t Blue);
    void ws2812_send_dma(TIM_HandleTypeDef *htim, uint32_t Channel, WS2812B *ws2812);
    void breathing_lamps_demo(TIM_HandleTypeDef *htim, uint32_t Channel, WS2812B *ws2812);
    /* USER CODE END EC */

    /* Exported macro ------------------------------------------------------------*/
    /* USER CODE BEGIN EM */

    /* USER CODE END EM */

    /* Exported functions prototypes ---------------------------------------------*/

    /* USER CODE BEGIN EFP */

    /* USER CODE END EFP */

    /* Private defines -----------------------------------------------------------*/

    /* USER CODE BEGIN Private defines */

    /* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __WS2812B_H */
