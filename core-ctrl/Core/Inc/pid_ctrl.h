/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : pid_ctrl.h
 * @brief          : Header for pid_ctrl.c file.
 *                   This file contains the common defines of the application.
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2023 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PID_CTRL_H
#define __PID_CTRL_H

#ifdef __cplusplus
extern "C"
{
#endif

    /* Includes ------------------------------------------------------------------*/

    /* Private includes ----------------------------------------------------------*/
    /* USER CODE BEGIN Includes */
#include "stm32f4xx_hal.h"
    /* USER CODE END Includes */

    /* Exported types ------------------------------------------------------------*/
    /* USER CODE BEGIN ET */

    /**
     * @brief PID_TypeDef
     * @param set_point 目标值
     * @param actualvalue   期望值
     * @param Kp    比例系数P
     * @param Ki    积分系数I
     * @param Kd    微分系数D
     * @param sum_error 偏差累积
     * @param error 第n次偏差
     * @param last_error    第n-1次偏差
     * @param prev_error    第n-2次偏差
     */
    typedef struct
    {
        __IO float set_point;
        __IO float actualvalue;
        __IO float Kp;
        __IO float Ki;
        __IO float Kd;
        __IO float sum_error;
        __IO float error;
        __IO float last_error;
        __IO float prev_error;
    } PID_TypeDef;

    float pid_location_style_ctrl(PID_TypeDef *PID, float input, float integral_limit);
    float pid_incremental_ctrl(PID_TypeDef *PID, float input);

    /* USER CODE END ET */

    /* Exported constants --------------------------------------------------------*/
    /* USER CODE BEGIN EC */

    /* USER CODE END EC */

    /* Exported macro ------------------------------------------------------------*/
    /* USER CODE BEGIN EM */

    /* USER CODE END EM */

    /* Exported functions prototypes ---------------------------------------------*/

    /* USER CODE BEGIN EFP */

    /* USER CODE END EFP */

    /* Private defines -----------------------------------------------------------*/

    /* USER CODE BEGIN Private defines */

    /* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __PID_CTRL_H */
