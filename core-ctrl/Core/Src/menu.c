#include "menu.h"
#include "stm32f4xx_hal.h"
#include "stdio.h"
#include "stdlib.h"
#include "log.h"
#include "st7735.h"

const static char *TAG = "menu.c";

/**
 * @brief 菜单结构体
 */
typedef struct
{
    uint8_t current;                       /* 当前组索引号 */
    uint8_t up;                            /* 上翻索引号 */
    uint8_t down;                          /* 下翻索引号 */
    uint8_t enter;                         /* 确认索引号 */
    void (*operation)(menu_show_data_t *); /* 动作 */
} menu_key_table_t;

menu_key_table_t table[];

/**
 * @brief 菜单
 * @param None
 * @retval None
 */
void main_menu(KEY_MSG *key_msg, menu_show_data_t *menu_data)
{
    static uint8_t index = 0;
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_SET);
    if (key_msg->pressed)
    {
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_RESET);
        key_msg->pressed = false;
        switch (key_msg->id)
        {
        case 0:
            MY_LOGI(TAG, "KEY Valu-> %d\r\n", key_msg->id);
            index = table[index].up; /* 上一个 */
            break;
        case 1:
            MY_LOGI(TAG, "KEY Valu-> %d\r\n", key_msg->id);
            index = table[index].enter;      /* 确认 */
            ST7735_FillScreen(ST7735_WHITE); /* init screen bgcolor */
            break;
        case 2:
            MY_LOGI(TAG, "KEY Valu-> %d\r\n", key_msg->id);
            index = table[index].down; /* 下一个 */
            break;
        default:
            break;
        }
    }
    if (table[index].operation != NULL)
    {
        table[index].operation(menu_data);
    }
}

/**
 * @brief Main
 * @param None
 * @retval None
 */
void start_gui(menu_show_data_t *menu_data)
{
    char string[13] = " ";
    ST7735_WriteString(0, 10, "  ArsSama  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "           ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "  My menu  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "           ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 50, "UI Designed", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    sprintf(string, "a_dat:%5d", menu_data->menu_a.a_num);
    ST7735_WriteString(0, 70, string, Font_7x10, ST7735_BLACK, ST7735_WHITE);
    sprintf(string, "_ch1:%.3f", menu_data->menu_a.adc_channel1);
    ST7735_WriteString(0, 90, string, Font_7x10, ST7735_BLACK, ST7735_YELLOW);
    sprintf(string, "_ch2:%.3f", menu_data->menu_a.adc_channel2);
    ST7735_WriteString(0, 100, string, Font_7x10, ST7735_BLACK, ST7735_YELLOW);
    // free(str);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_a(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "weater     ", Font_7x10, ST7735_WHITE, ST7735_BLACK);
    ST7735_WriteString(0, 20, "music      ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "device     ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "return...  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_b(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "weater     ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "music      ", Font_7x10, ST7735_WHITE, ST7735_BLACK);
    ST7735_WriteString(0, 30, "device     ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "return...  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_c(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "weater     ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "music      ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "device     ", Font_7x10, ST7735_WHITE, ST7735_BLACK);
    ST7735_WriteString(0, 40, "return...  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_d(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "weater     ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "music      ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "device     ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "return...  ", Font_7x10, ST7735_WHITE, ST7735_BLACK);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_a_a(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "HangZhou   ", Font_7x10, ST7735_WHITE, ST7735_BLACK);
    ST7735_WriteString(0, 20, "BeiJing    ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "ShangHai   ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "return...  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_a_b(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "HangZhou   ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "BeiJing    ", Font_7x10, ST7735_WHITE, ST7735_BLACK);
    ST7735_WriteString(0, 30, "ShangHai   ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "return...  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_a_c(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "HangZhou   ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "BeiJing    ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "ShangHai   ", Font_7x10, ST7735_WHITE, ST7735_BLACK);
    ST7735_WriteString(0, 40, "return...  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_a_d(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "HangZhou   ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "BeiJing    ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "ShangHai   ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "return...  ", Font_7x10, ST7735_WHITE, ST7735_BLACK);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_b_a(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "WindyHill  ", Font_7x10, ST7735_WHITE, ST7735_BLACK);
    ST7735_WriteString(0, 20, "New Boy    ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "Kiss Rain  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "return...  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_b_b(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "WindyHill  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "New Boy    ", Font_7x10, ST7735_WHITE, ST7735_BLACK);
    ST7735_WriteString(0, 30, "Kiss Rain  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "return...  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_b_c(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "WindyHill  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "New Boy    ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "Kiss Rain  ", Font_7x10, ST7735_WHITE, ST7735_BLACK);
    ST7735_WriteString(0, 40, "return...  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_b_d(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "WindyHill  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "New Boy    ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "Kiss Rain  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "return...  ", Font_7x10, ST7735_WHITE, ST7735_BLACK);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_c_a(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "WiFi       ", Font_7x10, ST7735_WHITE, ST7735_BLACK);
    ST7735_WriteString(0, 20, "Sound      ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "Version    ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "return...  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_c_b(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "WiFi       ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "Sound      ", Font_7x10, ST7735_WHITE, ST7735_BLACK);
    ST7735_WriteString(0, 30, "Version    ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "return...  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_c_c(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "WiFi       ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "Sound      ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "Version    ", Font_7x10, ST7735_WHITE, ST7735_BLACK);
    ST7735_WriteString(0, 40, "return...  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_c_d(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "WiFi       ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "Sound      ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "Version    ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "return...  ", Font_7x10, ST7735_WHITE, ST7735_BLACK);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_a_a_(menu_show_data_t *menu_data)
{
    char string[6] = " ";
    sprintf(string, "%3d C", menu_data->menu_b.b_num);
    ST7735_WriteString(0, 10, "HangZhou   ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "winding    ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "2023-7-5   ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(20+4, 80+4, string, Font_7x10, ST7735_BLACK, ST7735_YELLOW);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_a_b_(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "BeiJing    ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "rain       ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "2023-7-5   ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "25 C       ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_a_c_(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "ShangHai   ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "snowing    ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "2023-7-5   ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "35 C       ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_b_a_(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "WindyHill  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "           ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "           ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "           ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_b_b_(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "New Boy    ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "           ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "           ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "           ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_b_c_(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "play Kiss  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "           ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "           ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "           ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_c_a_(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "WiFi imfor ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "           ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "           ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "           ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_c_b_(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "Sound:%40  ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "           ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "           ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "           ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
}

/**
 * @brief 项
 * @param None
 * @retval None
 */
void gui_c_c_(menu_show_data_t *menu_data)
{
    ST7735_WriteString(0, 10, "Version 1.3", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 20, "           ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 30, "           ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
    ST7735_WriteString(0, 40, "           ", Font_7x10, ST7735_BLACK, ST7735_WHITE);
}

/**
 * @brief 菜单列表
 */
menu_key_table_t table[] =
    {
        // 第0层
        {0, 0, 0, 1, start_gui},

        // 第一层
        {1, 4, 2, 5, gui_a},
        {2, 1, 3, 9, gui_b},
        {3, 2, 4, 13, gui_c},
        {4, 3, 1, 0, gui_d},

        // 第二层
        {5, 8, 6, 17, gui_a_a},
        {6, 5, 7, 18, gui_a_b},
        {7, 6, 8, 19, gui_a_c},
        {8, 7, 5, 1, gui_a_d},

        {9, 12, 10, 20, gui_b_a},
        {10, 9, 11, 21, gui_b_b},
        {11, 10, 12, 22, gui_b_c},
        {12, 11, 9, 2, gui_b_d},

        {13, 16, 14, 23, gui_c_a},
        {14, 13, 15, 24, gui_c_b},
        {15, 14, 16, 25, gui_c_c},
        {16, 15, 13, 3, gui_c_d},

        // 第三层
        {17, 17, 17, 5, gui_a_a_},
        {18, 18, 18, 6, gui_a_b_},
        {19, 19, 19, 7, gui_a_c_},

        {20, 20, 20, 9, gui_b_a_},
        {21, 21, 21, 10, gui_b_b_},
        {22, 22, 22, 11, gui_b_c_},

        {23, 23, 23, 13, gui_c_a_},
        {24, 24, 24, 14, gui_c_b_},
        {25, 25, 25, 15, gui_c_c_},
};
