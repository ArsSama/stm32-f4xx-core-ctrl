#include "log.h"
#include "stm32f4xx_hal.h"
#include <stdio.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "stdarg.h"
#include "mutex.h"

/**
 * @brief my_log_printf
 * @note  自定义的日志输出函数
 * @param tag
 * @param level
 * @param format
 * @param ...
 */
void my_log_printf(const char *tag, my_log_level_t level, const char *format, ...)
{
    // 获取互斥锁
    xSemaphoreTake(logMutex, portMAX_DELAY);

    // 输出日志级别和标签
    printf("[%s]", tag);
    switch (level)
    {
    case MY_LOG_ERROR:
        printf("[E]");
        break;
    case MY_LOG_WARN:
        printf("[W]");
        break;
    case MY_LOG_INFO:
        printf("[I]");
        break;
    case MY_LOG_DEBUG:
        printf("[D]");
        break;
    case MY_LOG_VERBOSE:
        printf("[V]");
        break;
    default:
        break;
    }

    // 输出格式化字符串
    va_list args;
    va_start(args, format);
    vprintf(format, args);
    va_end(args);

    // 释放互斥锁
    xSemaphoreGive(logMutex);
}

/**
 * @brief error_handler_msg_log
 *
 * @param msg error message
 */
void error_handler_msg_log(char *file_msg, const char *func_msg)
{
    /* Start peintf error message, don't use rtos_log  */
    printf("Error file      : %s\r\n", file_msg);
    printf("Error function  : %s\r\n", func_msg);
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void error_handler(void)
{
    /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */
    __disable_irq();
    while (1)
    {
        NVIC_SystemReset(); /* system start reset */
    }
    /* USER CODE END Error_Handler_Debug */
}
