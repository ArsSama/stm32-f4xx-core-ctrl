#include "mutex.h"

// 定义互斥锁
SemaphoreHandle_t logMutex;  /*log资源互斥锁*/

/**
 * @brief create_mutex
 * @note  -- 创建互斥锁
 */
void create_mutex(void)
{
    logMutex = xSemaphoreCreateMutex();  /*创建log资源的互斥锁，防止日志打印资源冲突*/
}
