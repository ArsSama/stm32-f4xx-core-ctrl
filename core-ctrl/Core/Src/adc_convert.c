#include "adc_convert.h"
#include "stdio.h"
#include "log.h"

const static char *TAG = "adc_convert.c";

/**
 * @brief filter
 *
 * @param filter_data
 * @return HAL_StatusTypeDef
 */
static HAL_StatusTypeDef filter(uint32_t *filter_data)
{
    if (*filter_data == 0)
    {
        return HAL_ERROR;
    }
    else
    {
        /* filter proc begin */
        MY_LOGI(TAG, "filter_data:%d\r\n", *filter_data);
        *filter_data = *filter_data / 2;
        /* filter proc end */
        return HAL_OK;
    }
}

/**
 * @brief convert
 *
 * @param convert_data
 */
void convert(ADC_CONVERT *convert_data)
{
    if (filter(&convert_data->adc_data[0]) != HAL_OK)
    {
        /* error handle proc begin */
        error_handler_msg_log(__FILE__, __func__);
        error_handler();
        /* error handle proc end */
    }
    MY_LOGI(TAG, "convert success.%d\r\n", convert_data->adc_data[0]);
}
